ngx_http_geoip2_module  by Hirantha Wijayawardena 04/04/2020

        creates variables with values from the maxmind geoip2 databases
        based on the client IP (default) or from a specific variable
        https://github.com/leev/ngx_http_geoip2_module
        
        Following libraries and rpm needed.
        libmaxminddb.x86_64
        libmaxminddb-devel.x86_64
        wget https://github.com/maxmind/geoipupdate/releases/download/v4.2.2/geoipupdate_4.2.2_linux_amd64.rpm
        
        264322
        eKBblhg6PWo3T2QE

ngx_brotli		by Hirantha Wijayawardena 04/04/2020

	Brotli is a generic-purpose lossless compression algorithm that
	compresses data using a combination of a modern variant of the LZ77 
	algorithm, Huffman coding and 2nd order context modeling, with a compression 
	ratio comparable to the best currently available general-purpose compression 
	methods. It is similar in speed with deflate but offers more dense compression.
	
	cd /home/centos/rpmbuild/SOURCES/; rm -rf ngx_brotli
	git clone https://github.com/google/ngx_brotli.git
	cd ngx_brotli && git submodule update --init
	cd /home/centos/rpmbuild/SPECS

nginx_cookie_flag_module	by Hirantha Wijayawardena 04/04/2020

	The Nginx module for adding cookie flag