load_module modules/ngx_http_geoip2_module.so;

user  nginx;
worker_processes auto;
worker_cpu_affinity auto;
worker_rlimit_nofile 65535;

error_log  /var/log/nginx/error.log warn;
pid        /var/run/nginx.pid;


events {
	multi_accept on;
    worker_connections  1024;
}


http {

	charset utf-8;
	sendfile on;
	tcp_nopush on;
	tcp_nodelay on;
#	server_tokens off;
	log_not_found off;
	types_hash_max_size 2048;
	client_max_body_size 16M;
	
    include       /etc/nginx/mime.types;
    default_type  application/octet-stream;
    
    log_format upstreamlog '[$time_local] $remote_addr $remote_user $server_name '
                           'to: $upstream_addr upstream_response_time: $upstream_response_time msec '
                           'request_time: $request_time msec "$request" ';

    log_format  main  '$remote_addr - $remote_user [$time_local] "$request" '
                      '$status $body_bytes_sent "$http_referer" '
                      '"$http_user_agent" "$http_x_forwarded_for"';

    access_log  /var/log/nginx/access.log  main;


    keepalive_timeout  65;

# gzip
	gzip on;
	gzip_vary on;
	gzip_static       on;
	gzip_proxied any;
	gzip_comp_level 6;
	gzip_min_length   512;
	gzip_types text/plain text/css application/json application/x-javascript application/xml application/xml+rss text/javascript application/javascript text/x-js application/x-shockwave-flash text/json;

# brotli
	brotli on;
	brotli_comp_level 6;
	brotli_min_length 512;
	brotli_types *;
    
# security headers
	add_header X-Frame-Options "SAMEORIGIN" always;
	add_header X-XSS-Protection "1; mode=block" always;
	add_header X-Content-Type-Options "nosniff" always;
	add_header Referrer-Policy "no-referrer-when-downgrade" always;
	add_header Content-Security-Policy "default-src 'self' http: https: data: blob: 'unsafe-inline'" always;
	add_header Strict-Transport-Security "max-age=31536000; includeSubDomains; preload" always;

# GeoIP2
    include /etc/nginx/conf.d/geoip.conf;
    
    
    include /etc/nginx/conf.d/default.conf;
}